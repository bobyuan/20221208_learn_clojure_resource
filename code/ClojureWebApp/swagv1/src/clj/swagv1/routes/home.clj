(ns swagv1.routes.home
  (:require
   [swagv1.layout :as layout]
   [swagv1.service.product :as product]
   [clojure.java.io :as io]
   [swagv1.middleware :as middleware]
   [ring.util.response]
   [ring.util.http-response :as response]))



(defn home-page [request]
  (layout/render request "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn initdb-page [request]
  (let [products (product/get-all-products)]
    (when (empty? products)
      (product/init-db))
    (layout/render request "initdb.html" {})))


(defn about-page [request]
  (layout/render request "about.html"))

(defn home-routes []
  [ "" 
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats
                 ]}
   ["/" {:get home-page}]
   ["/initdb" {:get initdb-page}]
   ["/about" {:get about-page}]])

