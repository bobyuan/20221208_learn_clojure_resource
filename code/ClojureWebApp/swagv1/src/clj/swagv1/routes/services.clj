(ns swagv1.routes.services
  (:require
    [reitit.swagger :as swagger]
    [reitit.swagger-ui :as swagger-ui]
    [reitit.ring.coercion :as coercion]
    [reitit.coercion.spec :as spec-coercion]
    [reitit.ring.middleware.muuntaja :as muuntaja]
    [reitit.ring.middleware.multipart :as multipart]
    [reitit.ring.middleware.parameters :as parameters]
    [swagv1.middleware.formats :as formats]
    [ring.util.http-response :refer :all]
    [clojure.java.io :as io]
    [swagv1.handler.product :as prdhandler]))

(defn service-routes []
  ["/api"
   {:coercion spec-coercion/coercion
    :muuntaja formats/instance
    :swagger {:id ::api}
    :middleware [;; query-params & form-params
                 parameters/parameters-middleware
                 ;; content-negotiation
                 muuntaja/format-negotiate-middleware
                 ;; encoding response body
                 muuntaja/format-response-middleware
                 ;; exception handling
                 coercion/coerce-exceptions-middleware
                 ;; decoding request body
                 muuntaja/format-request-middleware
                 ;; coercing response bodys
                 coercion/coerce-response-middleware
                 ;; coercing request parameters
                 coercion/coerce-request-middleware
                 ;; multipart
                 multipart/multipart-middleware]}

   ;; swagger documentation
   ["" {:no-doc true
        :swagger {:info {:title "my-api"
                         :description "https://cljdoc.org/d/metosin/reitit"}}}

    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
             {:url "/api/swagger.json"
              :config {:validator-url nil}})}]]

   ["/ping"
    {:get (constantly (ok {:message "pong"}))}]
   

   ["/math"
    {:swagger {:tags ["math"]}}

    ["/plus"
     {:get {:summary "plus with spec query parameters"
            :parameters {:query {:x int?, :y int?}}
            :responses {200 {:body {:total pos-int?}}}
            :handler (fn [{{{:keys [x y]} :query} :parameters}]
                       {:status 200
                        :body {:total (+ x y)}})}
      :post {:summary "plus with spec body parameters"
             :parameters {:body {:x int?, :y int?}}
             :responses {200 {:body {:total pos-int?}}}
             :handler (fn [{{{:keys [x y]} :body} :parameters}]
                        {:status 200
                         :body {:total (+ x y)}})}}]]

   ["/product"
    {:swagger {:tags ["product"]}}
    ["/"
     {:get {:summary "get all products"
            :parameters {:query {:maxnum pos-int?}}
            :responses {200 {:body vector?}}
            :handler (fn [{{{:keys [maxnum]} :query} :parameters}]
                       {:status 200
                        :body (prdhandler/get-all-products-handler maxnum)})}
      
      :post {:summary "create new product (no prd_id), or update existing product (with prd_id)"
             :parameters {:body {:product-params map?}}
             :responses {200 {:body {:prd_id pos-int?}}}
             :handler (fn [{{{:keys [product-params]} :body} :parameters}]
                        {:status 200
                         :body {:prd_id (prdhandler/save-product-handler product-params)}})}}]
      
    ["/:prd_id"
        {:get {:summary "get one product by its ID"
               :parameters {:path {:prd_id pos-int?}}
               :responses {200 {:body map?}}
               :handler (fn [{{{:keys [prd_id]} :path} :parameters}]
                          (let [rec-found (prdhandler/get-product-by-id-handler prd_id)]
                            (if (nil? rec-found)
                              {:status 404}
                              {:status 200 :body rec-found})))}

         :delete {:summary "delete one product by its ID"
                  :parameters {:path {:prd_id pos-int?}}
                  :responses {200 {:body {:success boolean?}}}
                  :handler (fn [{{{:keys [prd_id]} :path} :parameters}]
                             {:status 200
                              :body {:success (= 1 (prdhandler/delete-product-by-id-handler prd_id))}})}}]]
               
   ["/files"
    {:swagger {:tags ["files"]}}

    ["/upload"
     {:post {:summary "upload a file"
             :parameters {:multipart {:file multipart/temp-file-part}}
             :responses {200 {:body {:name string?, :size int?}}}
             :handler (fn [{{{:keys [file]} :multipart} :parameters}]
                        {:status 200
                         :body {:name (:filename file)
                                :size (:size file)}})}}]

    ["/download"
     {:get {:summary "downloads a file"
            :swagger {:produces ["image/png"]}
            :handler (fn [_]
                       {:status 200
                        :headers {"Content-Type" "image/png"}
                        :body (-> "public/img/warning_clojure.png"
                                  (io/resource)
                                  (io/input-stream))})}}]]])
