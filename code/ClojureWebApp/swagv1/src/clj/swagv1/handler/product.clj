(ns swagv1.handler.product
  (:require [clojure.string :as str]
            [clojure.tools.logging :as log]
            [swagv1.common.mycommons :as mycommons]
            [swagv1.service.product :as prdservice]))


(defn get-all-products-handler
  "RESTful API hander for getting all products (limited by maxnum)."
  [maxnum]
  (let [rec-limit (mycommons/safe-get-int maxnum 100)]
    (log/debug (str "Calling get-all-products-handler() with maxnum=" maxnum))
    (prdservice/get-all-products-limited rec-limit)))

(defn get-product-by-id-handler
  "RESTful API hander for getting one product by its id."
  [prd_id]
  (let [rec-id (mycommons/safe-get-int prd_id 0)]
    (log/debug (str "Calling get-product-by-id-handler() with prd_id=" rec-id))
    (if (> rec-id 0)
      (prdservice/find-product-by-id {:prd_id rec-id})
      nil)))

(defn get-product-by-name-handler
  "RESTful API hander for getting one product by its name."
  [name]
  (let [rec-name (mycommons/safe-get-str name "")]
    (log/debug (str "Calling get-product-by-name-handler() with prd_name=" rec-name))
    (if (not (str/blank? rec-name))
      (prdservice/find-product-by-name {:prd_name rec-name})
      nil)))

(defn save-product-handler
  "RESTful API hander for creating (no prd_id) or updating (with prd_id) existing product by given map."
  [product-params]
  (when-let [prd_id (get product-params :prd_id)]
    (assert (pos-int? prd_id) "prd_id must be a positive integer."))
  (assert (not (str/blank? (get product-params :prd_name))) "prd_name cannot be empty.")
  (assert (not (nil? (get product-params :prd_desc))) "prd_desc must exist.")
  (assert (not (nil? (get product-params :quantity))) "quantity must exist.")
  (assert (not (nil? (get product-params :price))) "price must exist.")
  (log/debug (str "Calling save-product-handler() with product-params=" product-params))
  (prdservice/save-product! product-params))

(defn delete-product-by-id-handler
  "RESTful API hander for deleting a product by its id."
  [id]
  (let [rec-id (mycommons/safe-get-int id 0)]
    (log/debug (str "Calling delete-product-by-id-handler() with prd_id=" rec-id))
    (if (> rec-id 0)
      (prdservice/delete-product-by-id! {:prd_id rec-id})
      0)))
