(ns swagv1.common.mycommons
  (:require [tupelo.parse :as tp]))


(defn safe-get-int
  "Safely get an integer value from input, accept any type."
  [any-input default-value]
  (if (nil? any-input)
    default-value
    (if (string? any-input)
      (tp/parse-int any-input :default default-value)
      (try
        (int any-input)
        (catch java.lang.ClassCastException cce default-value)))))

(defn safe-get-long
  "Safely get a long value from input, accept any type."
  [any-input default-value]
  (if (nil? any-input)
    default-value
    (if (string? any-input)
      (tp/parse-long any-input :default default-value)
      (try
        (long any-input)
        (catch java.lang.ClassCastException cce default-value)))))

(defn safe-get-double
  "Safely get a double value from input, accept any type."
  [any-input default-value]
  (if (nil? any-input)
    default-value
    (if (string? any-input)
      (tp/parse-double any-input :default default-value)
      (try
        (double any-input)
        (catch java.lang.ClassCastException cce default-value)))))

(defn safe-get-str
  "Safely get a string value from input, accept any type."
  [any-input default-value]
  (if (nil? any-input)
    default-value
    (if (string? any-input)
      any-input
      (try
        (str any-input)
        (catch java.lang.ClassCastException cce default-value)))))


;; -------------------------- format-plus -------------------------
;; https://clojuredocs.org/clojure.core/format
(defn- coerce-unformattable-types [args]
  (map (fn [x]
         (cond (instance? clojure.lang.BigInt x) (biginteger x)
               (instance? clojure.lang.Ratio x) (double x)
               :else x))
       args))

(defn format-plus 
  "Enhanced version of format, which can handle large integer and ratio."
  [fmt & args]
  (apply format fmt (coerce-unformattable-types args)))

