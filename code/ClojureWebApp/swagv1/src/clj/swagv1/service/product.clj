(ns swagv1.service.product
  (:require
   [next.jdbc :as jdbc]
   [java-time.local]
   [clojure.string :as str]
   [swagv1.db.core :refer [*db*] :as db]
   [swagv1.common.mycommons :as mycommons]))


(defn get-all-products
  "Get all products from DB, return an empty list if no record found."
  []
  (jdbc/with-transaction [t-conn *db*]
    (if-let [records-in-db (db/get-all-products t-conn {})]
      records-in-db
      [])))

(defn get-all-products-limited
  "Get all products from DB with limit, return an empty list if no record found."
  [limit]
  (jdbc/with-transaction [t-conn *db*]
    (if-let [records-in-db (db/get-all-products-limited t-conn {:limit (max limit 1)})]
      records-in-db
      [])))

(defn save-product!
  "Save product to DB by given properties (provided in params map).
   Perform update operation if ID is given, or create operation otherwise.
   Return the ID of the record if success, or 0 if failed."
  [params]
  (assert (not (str/blank? (:prd_name params))) "prd_name cannot be empty.")
  (assert (not (nil? (:prd_desc params))) "prd_desc must exist.")
  (assert (not (nil? (:quantity params))) "quantity must exist.")
  (assert (not (nil? (:price params))) "price must exist.")
  (jdbc/with-transaction [t-conn *db*]
    (let [prd_id (get params :prd_id)]
      (if (nil? prd_id)
        (do ;; create a new product
          (db/create-product! t-conn params)
          ;; get the new id.
          (let [rec-found (db/get-product-by-name t-conn params)]
            (if (nil? rec-found) 0 (get rec-found :prd_id))))
        (do ;; update an exisitng product
          (let [rec-id (mycommons/safe-get-int prd_id 0)]
            (if (and (< 0 rec-id) (= 1 (db/update-product! t-conn params)))
              rec-id
              0)))))))

(defn delete-product-by-id!
  "Delete an product in DB by given prd_id (provided in params map)."
  [params]
  (assert (int? (:prd_id params)) "prd_id must be an integer.")
  (jdbc/with-transaction [t-conn *db*]
    (db/delete-product-by-id! t-conn params)))

(defn delete-product-by-name!
  "Delete an product in DB by given prd_name (provided in params map)."
  [params]
  (assert (not (str/blank? (:prd_name params))) "prd_name cannot be empty.")
  (jdbc/with-transaction [t-conn *db*]
    (db/delete-product-by-name! t-conn params)))

(defn find-product-by-id
  "Find an product record in DB by given prd_id (provided in params map).
   Return nil if not found."
  [params]
  (assert (int? (:prd_id params)) "prd_id must be an integer.")
  (jdbc/with-transaction [t-conn *db*]
    (db/get-product-by-id t-conn params)))

(defn find-product-by-name
  "Find an product record in DB by given prd_name (provided in params map).
   Return nil if not found."
  [params]
  (assert (not (str/blank? (:prd_name params))) "prd_name cannot be empty.")
  (jdbc/with-transaction [t-conn *db*]
    (db/get-product-by-name t-conn params)))

(defn init-db
  "Initializa database, load the sample data."
  []
  (jdbc/with-transaction [t-conn *db*]
    (db/create-product-with-id!
     t-conn
     {:prd_id 1
      :prd_name "鼠标"
      :prd_desc "罗技的经典鼠标"
      :quantity 10
      :price 123.0})
    (db/create-product-with-id!
     t-conn
     {:prd_id 2
      :prd_name "键盘"
      :prd_desc "罗技的经典机械键盘"
      :quantity 20
      :price 234.0})
    (db/create-product-with-id!
     t-conn
     {:prd_id 3
      :prd_name "摄像头"
      :prd_desc "罗技的高清网络摄像头"
      :quantity 30
      :price 345.0})))