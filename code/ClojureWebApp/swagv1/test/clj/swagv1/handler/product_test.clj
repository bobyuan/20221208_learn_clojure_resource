(ns swagv1.handler.product-test
  (:require
   [clojure.test :refer :all]
   [swagv1.handler.product :as prdhandler]))


(deftest test-get-all-products-handler
  (let [products (prdhandler/get-all-products-handler 10)]
    (println "products: " products)
    (is (<= 0 (count products)))))


(deftest test-create-product-handler
  (let [product-params {:prd_name "键鼠套装（测试）"
                 :prd_desc "罗技的经典键盘+鼠标套装（测试）"
                 :quantity 30
                 :price 456.0}]

    ;; find the product in given name, delete it if it exists.
    (when-let [rec-found (prdhandler/get-product-by-name-handler (get product-params :prd_name))]
      (println "delete-product-by-id-handler(): " (get rec-found :prd_id))
      (is (= 1 (prdhandler/delete-product-by-id-handler (get rec-found :prd_id)))))

    ;; create product.
    (is (< 0 (prdhandler/create-product-handler product-params)))

    ;; update product.
    (when-let [rec-found (prdhandler/get-product-by-name-handler (get product-params :prd_name))]
      (let [product-params-new (assoc rec-found :prd_desc "修改描述", :quantity 40, :price 567.0)]
        (is (< 0 (prdhandler/update-product-handler product-params-new)))))
    
    ;; delete the product.
    (when-let [rec-found (prdhandler/get-product-by-name-handler (get product-params :prd_name))]
      (is (= 1 (prdhandler/delete-product-by-id-handler (get rec-found :prd_id))))))
  )

