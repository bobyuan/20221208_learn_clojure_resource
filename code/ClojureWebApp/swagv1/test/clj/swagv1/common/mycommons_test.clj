(ns swagv1.common.mycommons-test
  (:require
   [clojure.test :refer :all]
   [swagv1.common.mycommons :as mycommons]))

(deftest test-safe-get-int
  ;; parse as int.
  (is (= -32768 (mycommons/safe-get-int "-32768" 12345)))
  (is (= 0 (mycommons/safe-get-int "0" 12345)))
  (is (= 10 (mycommons/safe-get-int "10" 12345)))
  (is (= 32767 (mycommons/safe-get-int "32767" 12345)))
  (is (= 2147483647 (mycommons/safe-get-int "2147483647" 12345)))    ;; Integer/MAX_VALUE
  (is (= -2147483648 (mycommons/safe-get-int "-2147483648" 12345)))  ;; Integer/MIN_VALUE
  (is (= 12345 (mycommons/safe-get-int "2147483648" 12345)))         ;; overflow

  ;; pase as int failed, take default value.
  (is (= 12345 (mycommons/safe-get-int nil 12345)))
  (is (= 12345 (mycommons/safe-get-int "" 12345)))
  (is (= 12345 (mycommons/safe-get-int "10a" 12345)))

  ;; cast as int.
  (is (= -13 (mycommons/safe-get-int -13 12345)))
  (is (= 10 (mycommons/safe-get-int 10 12345)))
  (is (= 123 (mycommons/safe-get-int 123.456 12345)))

  ;; cast to int failed, take default value.
  (is (= 12345 (mycommons/safe-get-int true 12345)))
  (is (= 12345 (mycommons/safe-get-int false 12345))))

(deftest test-safe-get-long
  ;; parse as long
  (is (= -32768 (mycommons/safe-get-long "-32768" 12345)))
  (is (= 0 (mycommons/safe-get-long "0" 12345)))
  (is (= 10 (mycommons/safe-get-long "10" 12345)))
  (is (= 32767 (mycommons/safe-get-long "32767" 12345)))
  (is (= 2147483647 (mycommons/safe-get-long "2147483647" 12345)))
  (is (= 9223372036854775807 (mycommons/safe-get-long "9223372036854775807" 12345)))    ;; Long/MAX_VALUE
  (is (= -9223372036854775808 (mycommons/safe-get-long "-9223372036854775808" 12345)))  ;; Long/MIN_VALUE
  (is (= 12345 (mycommons/safe-get-long "9223372036854775808" 12345)))                  ;; overflow

  ;; pase as long failed, take default value.
  (is (= 12345 (mycommons/safe-get-long nil 12345)))
  (is (= 12345 (mycommons/safe-get-long "" 12345)))
  (is (= 12345 (mycommons/safe-get-long "10a" 12345)))

  ;; cast as long
  (is (= 10 (mycommons/safe-get-long 10 12345)))
  (is (= 123 (mycommons/safe-get-long 123.456 12345)))

  ;; cast to long failed, take default value.
  (is (= 12345 (mycommons/safe-get-long true 12345)))
  (is (= 12345 (mycommons/safe-get-long false 12345))))

(deftest test-safe-get-double
  ;; parse as double
  (is (= -3.14159 (mycommons/safe-get-double "-3.14159" 12345)))
  (is (= 0.0 (mycommons/safe-get-double "0" 12345)))
  (is (= 0.0 (mycommons/safe-get-double "0.0" 12345)))
  (is (= 3.1415953589793237 (mycommons/safe-get-double "3.141595358979323846" 12345)))
  (is (= 3.1415953589793e+10 (mycommons/safe-get-double "3.1415953589793e+10" 12345)))
  
  ;; pase as double failed, take default value.
  (is (= 12345 (mycommons/safe-get-double nil 12345)))
  (is (= 12345 (mycommons/safe-get-double "" 12345)))
  (is (= 12345 (mycommons/safe-get-double "10a" 12345)))

  ;; cast as double
  (is (= 10.0 (mycommons/safe-get-double 10 12345)))
  (is (= 123.456 (mycommons/safe-get-double 123.456 12345)))

  ;; cast to double failed, take default value.
  (is (= 12345 (mycommons/safe-get-double true 12345)))
  (is (= 12345 (mycommons/safe-get-double false 12345))))

(deftest test-safe-get-str
  ;; cast to str.
  (is (= "hello" (mycommons/safe-get-str "hello" nil)))
  (is (= "你好" (mycommons/safe-get-str "你好" nil)))
  (is (= "" (mycommons/safe-get-str "" nil)))
  (is (= " " (mycommons/safe-get-str " " nil)))
    
  ;; cast to str (corner cases).
  (is (= "default" (mycommons/safe-get-str nil "default")))

  ;; cast to str (other types).
  (is (= "123" (mycommons/safe-get-str 123 nil)))
  (is (= "123.45" (mycommons/safe-get-str 123.45 nil)))
  (is (= "true" (mycommons/safe-get-str true nil)))
  (is (= "false" (mycommons/safe-get-str false nil))))


(deftest test-format-plus
  ;; same behavior as format.
  (is (= "Hello there, bob" (mycommons/format-plus "Hello there, %s" "bob")))
  (is (= "    3" (mycommons/format-plus "%5d" 3)))
  (is (= "Pad with leading zeros 0005432" (mycommons/format-plus "Pad with leading zeros %07d" 5432)))
  (is (= "Left justified :5432   :" (mycommons/format-plus "Left justified :%-7d:" 5432)))
  (is (= "Locale-specific group separators    1,234,567" (mycommons/format-plus (format "Locale-specific group separators %,12d" 1234567))))
  (is (= "decimal 63  octal 77  hex 3f  upper-case hex 3F" (mycommons/format-plus "decimal %d  octal %o  hex %x  upper-case hex %X" 63 63 63 63)))
  (is (= "23 Positional arguments" (mycommons/format-plus "%2$d %1$s" "Positional arguments" 23)))
  (is (= "2.000" (mycommons/format-plus "%.3f" 2.0)))
  
  ;; To use the percent sign % you need to escape it with %: %%
  (is (= "2.50 %" (mycommons/format-plus "%.2f %%" 2.5)))
  
  ;; with plus formats (bigint and ratio).
  (is (= "12345678901234567890" (mycommons/format-plus "%5d" 12345678901234567890)))
  (is (= "3.142857" (mycommons/format-plus "%.6f" (/ 22 7))))
)