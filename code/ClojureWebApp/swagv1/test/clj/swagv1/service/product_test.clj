(ns swagv1.service.product-test
  (:require
   [clojure.test :refer :all]
   [swagv1.service.product :as prdservice]))

(deftest test-products-basic
  (let [product2 {:prd_name "product2", :prd_desc "Description of product2", :quantity 20, :price 234.0}]
    ;; delete-product-by-name
    (prdservice/delete-product-by-name! {:prd_name "product2"})
    
    ;; save-product!
    (is (< 0 (prdservice/save-product! product2)))

    ;; get-all-products
    (is (<= 1 (count (prdservice/get-all-products))))

    ;; find-product-by-name
    (is (=
         {:prd_name "product2"
          :prd_desc "Description of product2"
          :quantity 20
          :price 234.0}
         (select-keys (prdservice/find-product-by-name {:prd_name "product2"})
                      [:prd_name :prd_desc :quantity :price])))

    ;; find-product-by-id
    (let [product2_id (:prd_id (prdservice/find-product-by-name {:prd_name "product2"}))]
      (is (not (nil? (prdservice/find-product-by-id {:prd_id product2_id}))))

      ;; delete-product-by-name!
      (prdservice/delete-product-by-name! {:prd_name "product2"})
      (is (nil? (prdservice/find-product-by-name {:prd_name "product2"})))

      ;; find-product-by-id
      (is (nil? (prdservice/find-product-by-id {:prd_id product2_id})))
    )
))


