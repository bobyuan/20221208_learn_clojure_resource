(ns swagv1.db.core-test
  (:require
   [swagv1.db.core :refer [*db*] :as db]
   [java-time.pre-java8]
   [luminus-migrations.core :as migrations]
   [clojure.test :refer :all]
   [next.jdbc :as jdbc]
   [swagv1.config :refer [env]]
   [mount.core :as mount]))

(use-fixtures
  :once
  (fn [f]
    (mount/start
     #'swagv1.config/env
     #'swagv1.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))

(deftest test-users
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    (is (= 1 (db/create-user!
              t-conn
              {:id         "1"
               :first_name "Sam"
               :last_name  "Smith"
               :email      "sam.smith@example.com"
               :pass       "pass"}
              {})))
    (is (= {:id         "1"
            :first_name "Sam"
            :last_name  "Smith"
            :email      "sam.smith@example.com"
            :pass       "pass"
            :admin      nil
            :last_login nil
            :is_active  nil}
           (db/get-user t-conn {:id "1"} {})))))


(deftest test-products-1
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    ;; create and retrieve
    (is (= 1 (db/create-product-with-id!
              t-conn
              {:prd_id 101
               :prd_name "product1"
               :prd_desc "Description of product1"
               :quantity 10
               :price 123.0})))
    (is (=
         {:prd_name "product1"
          :prd_desc "Description of product1"
          :quantity 10
          :price 123.0}
         (select-keys (db/get-product-by-id t-conn {:prd_id 101})
                      [:prd_name :prd_desc :quantity :price])))

    ;; update
    (is (= 1 (db/update-product!
              t-conn
              {:prd_id 101
               :prd_name "product1 (updated)"
               :prd_desc "Description of product1 (updated)"
               :quantity 20
               :price 234.0})))
    (is (=
         {:prd_name "product1 (updated)"
          :prd_desc "Description of product1 (updated)"
          :quantity 20
          :price 234.0}
         (select-keys (db/get-product-by-id t-conn {:prd_id 101})
                      [:prd_name :prd_desc :quantity :price])))

    ;; delete
    (is (= 1 (db/delete-product-by-id! t-conn {:prd_id 101})))
    (is (nil? (db/get-product-by-id t-conn {:prd_id 101})))))


(deftest test-products-2
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    ;; create 3 records
    (is (= 1 (db/create-product!
              t-conn
              {:prd_name "product2"
               :prd_desc "Description of product2"
               :quantity 20
               :price 234.0})))
    (is (= 1 (db/create-product!
              t-conn
              {:prd_name "product3"
               :prd_desc "Description of product3"
               :quantity 30
               :price 345.0})))
    (is (= 1 (db/create-product!
              t-conn
              {:prd_name "product4"
               :prd_desc "Description of product4"
               :quantity 40
               :price 456.0})))

    (is (<= 3 (count (db/get-all-products t-conn {}))))
    
    (is (not (empty? (db/get-product-by-name t-conn {:prd_name "product2"}))))
    (is (not (empty? (db/get-product-by-name t-conn {:prd_name "product3"}))))
    (is (not (empty? (db/get-product-by-name t-conn {:prd_name "product4"}))))

    ;; delete all
    (is (<= 0 (db/delete-all-products! t-conn {})))
    (is (= 0 (count (db/get-all-products t-conn {}))))))