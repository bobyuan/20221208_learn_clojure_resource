CREATE TABLE products (
    prd_id serial NOT NULL,
    prd_name character varying(70) NOT NULL,
    prd_desc character varying(200),
    quantity integer NOT NULL,
    price real NOT NULL,
    CONSTRAINT product_pkey PRIMARY KEY (prd_id)
);


CREATE TABLE promotions (
    prm_id serial NOT NULL,
    prm_name character varying(80) NOT NULL,
    prm_desc character varying(200),
    starting_date timestamp without time zone NOT NULL,
    ending_date timestamp without time zone NOT NULL,
    product_related integer,
    CONSTRAINT promotion_pkey PRIMARY KEY (prm_id),
    CONSTRAINT promotion_product_related_fkey FOREIGN KEY (product_related)
        REFERENCES products (prd_id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
);