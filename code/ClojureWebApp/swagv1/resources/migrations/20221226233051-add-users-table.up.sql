CREATE TABLE users
(id VARCHAR(20) PRIMARY KEY,
 first_name VARCHAR(30),
 last_name VARCHAR(30),
 email VARCHAR(30),
 admin BOOLEAN,
 last_login TIMESTAMP,
 is_active BOOLEAN,
 pass VARCHAR(300));

-----------------------------------------------

CREATE TABLE products (
	prd_id INTEGER PRIMARY KEY AUTO_INCREMENT,
	prd_name VARCHAR(70) UNIQUE NOT NULL,
	prd_desc VARCHAR(200),
	quantity INTEGER NOT NULL DEFAULT 0,
	price REAL NOT NULL DEFAULT 0.0
);

CREATE TABLE promotions (
    prm_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    prm_name VARCHAR(100) UNIQUE NOT NULL,
    prm_desc VARCHAR(200),
    starting_date TIMESTAMP NOT NULL,
    ending_date TIMESTAMP NOT NULL,
    product_related INTEGER,
    CONSTRAINT promotion_product_related_fkey FOREIGN KEY (product_related)
        REFERENCES products (prd_id)
        ON UPDATE NO ACTION ON DELETE CASCADE
);
