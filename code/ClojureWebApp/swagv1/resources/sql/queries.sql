-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id


---------------------- products -----------------------------

-- :name create-product-with-id! :! :n
-- :doc creates a new product record by given the prd_id.
INSERT INTO products
(prd_id, prd_name, prd_desc, quantity, price)
VALUES (:prd_id, :prd_name, :prd_desc, :quantity, :price)

-- :name create-product! :! :n
-- :doc creates a new product record by generating the prd_id automatically.
INSERT INTO products
(prd_name, prd_desc, quantity, price)
VALUES (:prd_name, :prd_desc, :quantity, :price)

-- :name update-product! :! :n
-- :doc updates an existing product record by given the prd_id.
UPDATE products
SET prd_name = :prd_name, prd_desc = :prd_desc, quantity = :quantity, price = :price
WHERE prd_id = :prd_id

-- :name get-product-by-id :? :1
-- :doc retrieves a product record given the prd_id
SELECT * FROM products
WHERE prd_id = :prd_id

-- :name get-product-by-name :? :1
-- :doc retrieves a product record given the prd_name
SELECT * FROM products
WHERE prd_name = :prd_name

-- :name get-all-products :? :*
-- :doc retrieves all product records
SELECT * FROM products
ORDER BY prd_id DESC

-- :name get-all-products-limited :? :*
-- :doc retrieves all product records
SELECT * FROM products 
ORDER BY prd_id DESC
LIMIT :limit

-- :name delete-product-by-id! :! :n
-- :doc deletes a product record given the prd_id
DELETE FROM products
WHERE prd_id = :prd_id

-- :name delete-product-by-name! :! :n
-- :doc deletes a product record given the prd_name
DELETE FROM products
WHERE prd_name = :prd_name


-- :name delete-all-products! :! :n
-- :doc deletes all product records.
DELETE FROM products

---------------------- promotions -----------------------------
