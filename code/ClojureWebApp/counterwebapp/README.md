# README

This is a simple counter web application in Clojure. 



This project is generated using Luminus version "4.47"

```shell
lein new luminus counterwebapp +h2 +immutant
```



## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen



## Running

Run this in the root of the project to create the H2 database file:

```shell
# Initialize the database.
lein run migrate
```

To start a web server for the application, run:

```shell
# Run the application, default: http://localhost:3000/
lein run

# Run on another port.
#lein run --port 8080
```



It can also be packaged into release jar file and run. For example:

```shell
# Create the release jar file.
lein uberjar

# Set the required environment and run it.
SET database-url="jdbc:h2:./counterwebapp_dev.db"
java -Dport=3000 -jar target/uberjar/counterwebapp.jar
```



Wait for a few minutes, you will see log messages like below:

```
...
INFO  luminus.http-server - starting HTTP server on port 3000
INFO  org.projectodd.wunderboss.web.Web - Registered web context /
INFO  employees1.nrepl - starting nREPL server on port 7000
....
```

It means the http-server and nREPL server are started successfully. 



To access this web application: http://localhost:3000/



## Connect to REPL

To start the REPL by given port.

```shell
lein repl :start :port 7000
```

And then to connect to the REPL.

```shell
lein repl :connect localhost:7000
```



## License

Copyright © 2022 by Bob YUAN.

