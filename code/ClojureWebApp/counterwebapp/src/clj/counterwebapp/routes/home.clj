(ns counterwebapp.routes.home
  (:require
   [counterwebapp.layout :as layout]
   [clojure.java.io :as io]
   [counterwebapp.middleware :as middleware]
   [counterwebapp.service.counter :as counter]
   [ring.util.response]
   [ring.util.http-response :as response]))


(defn home-page [request]
  (layout/render request "home.html" 
                 {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn counter-page [request]
  (layout/render request "counter.html" 
                 {:counter_value (counter/get-next-counter-value)}))

(defn about-page [request]
  (layout/render request "about.html"))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/counter" {:get counter-page}]
   ["/about" {:get about-page}]])

