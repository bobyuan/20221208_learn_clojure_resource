(ns counterwebapp.service.counter
  (:require
   [counterwebapp.db.core :refer [*db*] :as db]
   [next.jdbc :as jdbc]))


(def COUNTER_NAME "WebCounter1")


(defn get-next-counter-value
  "Get the next counter value (will increase by 1)."
  []
  (jdbc/with-transaction [t-conn *db*]
    ;; get current counter value from DB, create the counter if needed.
    (let [record-in-db (db/get-counter-by-name t-conn {:counter_name COUNTER_NAME})
          counter-value (if (nil? record-in-db)
                                  (do
                                    (db/create-counter! t-conn {:counter_name COUNTER_NAME, 
                                                                :counter_value 0})
                                    0)
                                  (get record-in-db :counter_value))
          next-counter-value (inc counter-value)]
      ;; update DB and then return the increated new value.
      (db/update-counter-by-name! t-conn {:counter_name COUNTER_NAME, 
                                          :counter_value next-counter-value})
      next-counter-value)))
