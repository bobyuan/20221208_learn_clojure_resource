(ns counterwebapp.db.core-test
  (:require
   [counterwebapp.db.core :refer [*db*] :as db]
   [java-time.pre-java8]
   [luminus-migrations.core :as migrations]
   [clojure.test :refer :all]
   [next.jdbc :as jdbc]
   [counterwebapp.config :refer [env]]
   [mount.core :as mount]))

(use-fixtures
  :once
  (fn [f]
    (mount/start
     #'counterwebapp.config/env
     #'counterwebapp.db.core/*db*)
    (migrations/migrate ["migrate"] (select-keys env [:database-url]))
    (f)))

(deftest test-users
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    (is (= 1 (db/create-user!
              t-conn
              {:id         "1"
               :first_name "Sam"
               :last_name  "Smith"
               :email      "sam.smith@example.com"
               :pass       "pass"}
               {})))
    (is (= {:id         "1"
            :first_name "Sam"
            :last_name  "Smith"
            :email      "sam.smith@example.com"
            :pass       "pass"
            :admin      nil
            :last_login nil
            :is_active  nil}
           (db/get-user t-conn {:id "1"} {})))))


(def TEST_COUNTER_NAME "TestCounter1")

(deftest test-counters-create-update
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    ;; create.
    (is (= 1
           (db/create-counter! t-conn {:counter_name TEST_COUNTER_NAME
                                       :counter_value 123})))
    ;; get-by-name
    (let [rec-in-db (db/get-counter-by-name t-conn {:counter_name TEST_COUNTER_NAME})]
      (is (= {:counter_name TEST_COUNTER_NAME
              :counter_value 123}
             (select-keys rec-in-db [:counter_name :counter_value])))
      ;; get-by-id.
      (when-let [rec-id (get rec-in-db :counter_id)]
        (is (= rec-in-db
               (db/get-counter-by-id t-conn {:counter_id rec-id})))))

    ;; update-by-name
    (is (= 1
           (db/update-counter-by-name! t-conn {:counter_name TEST_COUNTER_NAME
                                               :counter_value 456})))
    ;; get-by-name
    (let [rec-in-db (db/get-counter-by-name t-conn {:counter_name TEST_COUNTER_NAME})]
      (is (= {:counter_value 456}
             (select-keys rec-in-db [:counter_value])))
      ;; update-by-id.
      (when-let [rec-id (get rec-in-db :counter_id)]
        (is (= 1
               (db/update-counter-by-id! t-conn {:counter_name TEST_COUNTER_NAME
                                                 :counter_value 789
                                                 :counter_id rec-id})))
        ;; get-by-id.
        (is (= {:counter_value 789}
               (select-keys
                (db/get-counter-by-id t-conn {:counter_id rec-id})
                [:counter_value])))))))


(deftest test-counters-delete-by-name
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    ;; create.
    (is (= 1
           (db/create-counter! t-conn {:counter_name TEST_COUNTER_NAME
                                       :counter_value 123})))
    ;; get-by-name.
    (is (= {:counter_name TEST_COUNTER_NAME
            :counter_value 123}
           (select-keys
            (db/get-counter-by-name t-conn {:counter_name TEST_COUNTER_NAME})
            [:counter_name :counter_value])))
    ;; delete-by-name, get-by-name.
    (is (= 1
           (db/delete-counter-by-name! t-conn {:counter_name TEST_COUNTER_NAME})))
    (is (nil?
         (db/get-counter-by-name t-conn {:counter_name TEST_COUNTER_NAME})))))


(deftest test-counters-delete-by-id
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    ;; create.
    (is (= 1
           (db/create-counter! t-conn {:counter_name TEST_COUNTER_NAME
                                       :counter_value 123})))
    ;; get-by-name.
    (let [rec-in-db (db/get-counter-by-name t-conn {:counter_name TEST_COUNTER_NAME})]
      (is (= {:counter_name TEST_COUNTER_NAME
              :counter_value 123}
             (select-keys rec-in-db [:counter_name :counter_value])))
      ;; delete-by-id, get-by-id.
      (when-let [rec-id (get rec-in-db :counter_id)]
        (is (= 1
               (db/delete-counter-by-id! t-conn {:counter_id  rec-id})))
        (is (nil?
             (db/get-counter-by-id t-conn {:counter_id rec-id})))))))
