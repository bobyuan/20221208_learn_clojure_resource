-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id

------------------------------------------------------------------------------

-- :name create-counter! :! :n
-- :doc creates a new counter record
INSERT INTO counters
(counter_name, counter_value)
VALUES (:counter_name, :counter_value)


-- :name get-counter-by-id :? :1
-- :doc retrieves a counter record given the id
SELECT * FROM counters
WHERE counter_id = :counter_id

-- :name get-counter-by-name :? :1
-- :doc retrieves a counter record given the name
SELECT * FROM counters
WHERE counter_name = :counter_name


-- :name update-counter-by-id! :! :n
-- :doc updates an existing counter record
UPDATE counters
SET counter_value = :counter_value, counter_name = :counter_name
WHERE counter_id = :counter_id

-- :name update-counter-by-name! :! :n
-- :doc updates an existing counter record
UPDATE counters
SET counter_value = :counter_value
WHERE counter_name = :counter_name


-- :name delete-counter-by-id! :! :n
-- :doc deletes a counter record given the id
DELETE FROM counters
WHERE counter_id = :counter_id

-- :name delete-counter-by-name! :! :n
-- :doc deletes a counter record given the name
DELETE FROM counters
WHERE counter_name = :counter_name
