# counterwebapp

Congratulations, this web application is running!

**[Try it out!](counter)**



This project is created using [Luminus](https://luminusweb.com/) as template, and having these third-party components:

* [H2](http://h2database.com/) as database
* [Selmer](https://github.com/yogthos/Selmer) as web template engine
* [lein-immutant](https://github.com/immutant/lein-immutant/) plugin to use [immutant](http://immutant.org/) as embedded application server.



## Deployment

### Create jar file

Generate the release jar file `target/uberjar/counterwebapp.jar`

```shell
lein uberjar
```

### Run as jar file

This web application needs only 1 environment variable to run, which is the DB connection parameter. 

Here for example, I have the H2 database file there and the connection string is as follows:

```shell
database-url="jdbc:h2:~/db/h2/counterwebapp_prod.db"
```

Then we can run this web application like this:

```shell
java -jar -Ddatabase-url="jdbc:h2:~/db/h2/counterwebapp_prod.db" target/uberjar/counterwebapp.jar
```



### Create docker image

To create a docker image:

```shell
docker build --tag bobyuan/counterwebapp .
```



### Run in docker

Here is a reference doc on passing environment to docker: 

https://www.baeldung.com/ops/docker-container-environment-variables



This web application needs only 1 environment variable to run, which is the DB connection URL. We can override this by using other DB connection string, like for example to MySQL DB.

```shell
database-url="jdbc:h2:/counterwebapp/db/h2/counterwebapp_prod.db"
```

We can pass environment variables as key-value pairs directly into the command line using the parameter *–env* (or its short form *-e*).

```shell
# test print out the environment of the docker run command.
docker run --rm --env database-url="jdbc:h2:/counterwebapp/db/h2/counterwebapp_prod.db" --publish 8080:3000 bobyuan/counterwebapp:latest env

# lanuch the docker run command.
docker run --env database-url="jdbc:h2:/counterwebapp/db/h2/counterwebapp_prod.db" --publish 8080:3000 bobyuan/counterwebapp:latest
```



If there are too many environment variables, a better way is to create a text file `myevn.txt` with all the environment variables listed in it, for example as below:

```bash
database-url="jdbc:h2:/counterwebapp/db/h2/counterwebapp_prod.db"
```

Then run in container like this:

```shell
# test print out the environment of the docker run command.
docker run --rm --env-file myenv.txt --publish 8080:3000 bobyuan/counterwebapp:latest env

# lanuch the docker run command.
docker run --env-file myenv.txt --publish 8080:3000 bobyuan/counterwebapp:latest
```





## Create this application

This project is generated using Luminus.

```shell
lein new luminus counterwebapp +h2 +immutant
```



A table is created to store the counter values in H2 database.

**resources/migrations/??-table.up.sql**

```sql
CREATE TABLE counters (
  counter_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  counter_name VARCHAR(100) NOT NULL UNIQUE,
  counter_value BIGINT UNSIGNED NOT NULL DEFAULT 0
);
```



And the related CRUD operations are defined in here:

**resources/sql/queries.sql**

```sql
-- :name create-counter! :! :n
-- :doc creates a new counter record
INSERT INTO counters
(counter_name, counter_value)
VALUES (:counter_name, :counter_value)


-- :name get-counter-by-id :? :1
-- :doc retrieves a counter record given the id
SELECT * FROM counters
WHERE counter_id = :counter_id

-- :name get-counter-by-name :? :1
-- :doc retrieves a counter record given the name
SELECT * FROM counters
WHERE counter_name = :counter_name

....
```

In the comment above the SQL script, are the functions for CRUD operations. 

These functions are tested in:

**test/clj/counterwebapp/db/core_test.clj**

```clojure
(def TEST_COUNTER_NAME "TestCounter1")

(deftest test-counters-create-update
  (jdbc/with-transaction [t-conn *db* {:rollback-only true}]
    ;; create.
    (is (= 1
           (db/create-counter! t-conn {:counter_name TEST_COUNTER_NAME
                                       :counter_value 123})))
    ;; get-by-name
    (let [rec-in-db (db/get-counter-by-name t-conn {:counter_name TEST_COUNTER_NAME})]
      (is (= {:counter_name TEST_COUNTER_NAME
              :counter_value 123}
             (select-keys rec-in-db [:counter_name :counter_value])))
      ;; get-by-id.
      (when-let [rec-id (get rec-in-db :counter_id)]
        (is (= rec-in-db
               (db/get-counter-by-id t-conn {:counter_id rec-id})))))

    ....
))
```

A service is created, 

**src/clj/counterwebapp/service/counter.clj**

```clojure
(def COUNTER_NAME "WebCounter1")


(defn get-next-counter-value
  "Get the next counter value (will increase by 1)."
  []
  (jdbc/with-transaction [t-conn *db*]
    ;; get current counter value from DB, create the counter if needed.
    (let [record-in-db (db/get-counter-by-name t-conn {:counter_name COUNTER_NAME})
          counter-value (if (nil? record-in-db)
                                  (do
                                    (db/create-counter! t-conn 
                                                        {:counter_name COUNTER_NAME, 
                                                         :counter_value 0})
                                    0)
                                  (get record-in-db :counter_value))
          next-counter-value (inc counter-value)]
      ;; update DB and then return the increated new value.
      (db/update-counter-by-name! t-conn {:counter_name COUNTER_NAME, 
                                          :counter_value next-counter-value})
      next-counter-value)))
```

where a function `get-next-counter-value` is created, and been used in user routes as the counter implementation.

**src/clj/counterwebapp/routes/home.clj**

```clojure
(defn counter-page [request]
  (layout/render request "counter.html" {:counter_value (counter/get-next-counter-value)}))


(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/counter" {:get counter-page}]
   ["/about" {:get about-page}]])
```

Hence the `/counter` URI will be handled by `counter-page` method, and this method will increase by 1 each time been visited (refreshed).



### Why are you seeing this page?

The `home-routes` handler in the `counterwebapp.routes.home` namespace defines the route that invokes the `home-page` function whenever an HTTP request is made to the `/` URI using the `GET` method.

```clojure
(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/counter" {:get counter-page}]
   ["/about" {:get about-page}]])
```

The `home-page` function will in turn call the `counterwebapp.layout/render` function to render the HTML content:

```clojure
(defn home-page [request]
  (layout/render
    request 
    "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))
```

The `render` function will render the `home.html` template found in the `resources/html` folder using a parameter map containing the `:docs` key. This key points to the contents of the `resources/docs/docs.md` file containing these instructions. 

The HTML templates are written using [Selmer](https://github.com/yogthos/Selmer) templating engine.

```html
<div class="content">
  {{docs|markdown}}
</div>
```

<a class="level-item button" href="https://luminusweb.com/docs/html_templating.html">learn more about HTML templating »</a>



Same logic applies to `/counter` URI using the `GET` method, it is handled in `counter-page` function.

```clojure
(defn counter-page [request]
  (layout/render request "counter.html" 
                 {:counter_value (counter/get-next-counter-value)}))
```

The `render` function will render the `counter.html` template found in the `resources/html`
folder using a parameter map containing the `:counter_value` key. This key will take a value from database. It will increate by 1 each time the page is visited.



### Organizing the routes

The routes are aggregated and wrapped with middleware in the `counterwebapp.handler` namespace:

```clojure
(mount/defstate app-routes
  :start
  (ring/ring-handler
    (ring/router
      [(home-routes)])
    (ring/routes
      (ring/create-resource-handler
        {:path "/"})
      (wrap-content-type
        (wrap-webjars (constantly nil)))
      (ring/create-default-handler
        {:not-found
         (constantly (error-page {:status 404, :title "404 - Page not found"}))
         :method-not-allowed
         (constantly (error-page {:status 405, :title "405 - Not allowed"}))
         :not-acceptable
         (constantly (error-page {:status 406, :title "406 - Not acceptable"}))}))))
```

The `app` definition groups all the routes in the application into a single handler.
A default route group is added to handle the `404` case.

<a class="level-item button" href="https://luminusweb.com/docs/routes.html">learn more about routing »</a>

The `home-routes` are wrapped with two middleware functions. The first enables CSRF protection. The second takes care of serializing and deserializing various encoding formats, such as JSON.



### Managing your middleware

Request middleware functions are located under the `counterwebapp.middleware` namespace.

This namespace is reserved for any custom middleware for the application. Some default middleware is
already defined here. The middleware is assembled in the `wrap-base` function.

Middleware used for development is placed in the `counterwebapp.dev-middleware` namespace found in
the `env/dev/clj/` source path.

<a class="level-item button" href="https://luminusweb.com/docs/middleware.html">learn more about middleware »</a>



### Database configuration is required

If you haven't already, then please follow the steps below to configure your database connection and run the necessary migrations.

* Run `lein run migrate` in the root of the project to create the tables.
* Let `mount` know to start the database connection by `require`-ing `counterwebapp.db.core` in some other namespace.
* Restart the application.

<a class="btn btn-primary" href="http://www.luminusweb.net/docs/database.md">learn more about database access »</a>



### Need some help?

Visit the [official documentation](https://luminusweb.com/docs/guestbook) for examples on how to accomplish common tasks with Luminus. The `#luminus` channel on the [Clojurians Slack](http://clojurians.net/) and [Google Group](https://groups.google.com/forum/#!forum/luminusweb) are both great places to seek help and discuss projects with other users.