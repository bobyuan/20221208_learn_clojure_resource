(ns counterwebapp.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [counterwebapp.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[counterwebapp started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[counterwebapp has shut down successfully]=-"))
   :middleware wrap-dev})
