# helloctmx

This is a simple web application showing basics of HTMX using ctmx package.

generated using Luminus version "4.47"


## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

```shell
lein run 
```

Open a browser to:  http://localhost:3000/

## License

Copyright © 2022 Bob YUAN
