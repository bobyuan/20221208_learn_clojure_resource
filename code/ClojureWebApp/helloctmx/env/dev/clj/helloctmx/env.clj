(ns helloctmx.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [helloctmx.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[helloctmx started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[helloctmx has shut down successfully]=-"))
   :middleware wrap-dev})
