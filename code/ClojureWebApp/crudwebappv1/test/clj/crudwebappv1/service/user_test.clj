(ns crudwebappv1.db.core-test
  (:require
   [clojure.test :refer :all]
   [crudwebappv1.service.user :as user]
   ))


(deftest test-get-all-users
  (is (<= 1 (count (user/get-all-users)))))



