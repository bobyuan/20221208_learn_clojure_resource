-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id


---------------------------------------------------

-- :name create-employee-with-id! :! :n
-- :doc creates a new employee record by given the emp_id.
INSERT INTO employees
(emp_id, full_name, position, office, email, gender, is_active, salary, birthday, notes)
VALUES (:emp_id, :full_name, :position, :office, :email, :gender, :is_active, :salary, :birthday, :notes)

-- :name create-employee! :! :n
-- :doc creates a new employee record by generating the id automatically.
INSERT INTO employees
(full_name, position, office, email, gender, is_active, salary, birthday, notes)
VALUES (:full_name, :position, :office, :email, :gender, :is_active, :salary, :birthday, :notes)


-- :name update-employee! :! :n
-- :doc updates an existing employee record given the emp_id.
UPDATE employees
SET full_name = :full_name, position = :position, office = :office, email = :email, gender = :gender, 
    is_active = :is_active, salary = :salary, birthday = :birthday, notes = :notes, time_updated = NOW()
WHERE emp_id = :emp_id

-- :name get-all-employees :? :*
-- :doc retrieves all employee records.
SELECT * FROM employees
ORDER BY time_updated DESC

-- :name get-employee :? :1
-- :doc retrieves a employee record given the emp_id.
SELECT * FROM employees
WHERE emp_id = :emp_id

-- :name delete-employee! :! :n
-- :doc deletes a employee record given the emp_id.
DELETE FROM employees
WHERE emp_id = :emp_id

-- :name delete-all-employees! :! :n
-- :doc deletes all employee records.
DELETE FROM employees
