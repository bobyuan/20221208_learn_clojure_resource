CREATE TABLE users
(id VARCHAR(20) PRIMARY KEY,
 first_name VARCHAR(30),
 last_name VARCHAR(30),
 email VARCHAR(30),
 admin BOOLEAN,
 last_login TIMESTAMP,
 is_active BOOLEAN,
 pass VARCHAR(300));

-----------------------------------------------

CREATE TABLE employees
(emp_id INT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 full_name VARCHAR(50),
 position VARCHAR(50),
 office VARCHAR(50),
 email VARCHAR(30),
 gender VARCHAR(1),
 is_active BOOLEAN,
 salary NUMBER(12,2),
 birthday DATE,
 notes VARCHAR(200),
 time_created DATETIME DEFAULT NOW() NOT NULL,
 time_updated DATETIME DEFAULT NOW() NOT NULL);
 