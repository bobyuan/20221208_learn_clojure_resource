Place AdminLTE release files here:
* DIR: dist, pages, plugins
* FILE: *.html
  include: iframe.html, iframe-dark.html, index.html, index2.html, index3.html, starter.html

https://github.com/ColorlibHQ/AdminLTE/releases
Version:
AdminLTE v3.2.0 Latest
