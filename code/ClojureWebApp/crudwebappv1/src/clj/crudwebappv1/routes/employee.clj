(ns crudwebappv1.routes.employee
  (:require
   [crudwebappv1.layout :as layout]
   [crudwebappv1.service.employee :as employee]
   [clojure.java.io :as io]
   [crudwebappv1.middleware :as middleware]
   [ring.util.response]
   [ring.util.http-response :as response]
   [struct.core :as st]))


(def employee-schema
  [[:full_name st/required st/string
    {:message "must contain at least 2 characters"
     :validate #(>= (count %) 2)}]
   [:position st/string]
   [:office st/string]
   [:email st/email]
   [:gender st/string]
   [:is_active st/boolean-str]
   [:salary st/number-str]
   [:birthday st/string]
   [:notes st/string]])


(defn validate-employee [params]
  (first (st/validate params employee-schema)))

;; ------ init ------

(defn initdb-page [request]
  (let [employees (employee/get-all-employees)]
    (when (empty? employees)
      (employee/init-db))
    (layout/render request "initdb.html" {})))

;; ------ list ------

(defn employee-list-page [request]
  (let [employees (employee/get-all-employees)]
    (layout/render request "employee.html" {:employees employees})))

;; ------ create ------

(defn employee-create-page [{:keys [flash] :as request}]
  (layout/render request "employee_create.html"
                 (select-keys flash [:full_name :position :office :email :gender :is_active :salary :birthday :notes
                                     :errors])))

(defn employee-create-action! [{:keys [params]}]
  (if-let [errors (validate-employee params)]
    (-> (response/found "/employee_create")
        (assoc :flash (assoc params :errors errors)))
    (do
      (employee/save-employee! params)
      (response/found "/employee"))))

;; ------ update ------

(defn employee-update-page [{:keys [flash params] :as request}]
  (let [flashMap (select-keys flash [:emp_id :full_name :position :office :email :gender :is_active :salary :birthday :notes :errors])
        idInFlashMap (get flashMap :emp_id)]
    (if (nil? idInFlashMap)
      (do
        (assert (not (nil? (:emp_id params))) "emp_id must be provided in requst params.")
        (layout/render request "employee_update.html" (employee/find-employee-by-id params)))
      (layout/render request "employee_update.html" flashMap))))

(defn employee-update-action! [{:keys [params]}]
  (if-let [errors (validate-employee params)]
    (-> (response/found "/employee_update")
        (assoc :flash (assoc params :errors errors)))
    (do
      (employee/save-employee! params)
      (response/found "/employee"))))

;; ------ delete ------

(defn employee-delete-action! [{:keys [params]}]
  (employee/delete-employee-by-id! params)
  (response/found "/employee"))


(defn employee-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/initdb" {:get initdb-page}]
   ["/employee" {:get employee-list-page}]
   ["/employee_create" {:get employee-create-page
                        :post employee-create-action!}]
   ["/employee_update" {:get employee-update-page
                        :post employee-update-action!}]
   ["/employee_delete" {:get employee-delete-action!}]])

