(ns crudwebappv1.handler
  (:require
    [crudwebappv1.middleware :as middleware]
    [crudwebappv1.layout :refer [error-page]]
    [crudwebappv1.routes.home :refer [home-routes]]
    [crudwebappv1.routes.employee :refer [employee-routes]]
    [reitit.ring :as ring]
    [ring.middleware.content-type :refer [wrap-content-type]]
    [ring.middleware.webjars :refer [wrap-webjars]]
    [crudwebappv1.env :refer [defaults]]
    [mount.core :as mount]))

(mount/defstate init-app
  :start ((or (:init defaults) (fn [])))
  :stop  ((or (:stop defaults) (fn []))))

(defn- async-aware-default-handler
  ([_] nil)
  ([_ respond _] (respond nil)))


(mount/defstate app-routes
  :start
  (ring/ring-handler
    (ring/router
      [(home-routes) (employee-routes)])
    (ring/routes
      (ring/create-resource-handler
        {:path "/"})
      (ring/create-resource-handler
       {:path "/" :root "addons/adminlte"})     
      (wrap-content-type
        (wrap-webjars async-aware-default-handler))
      (ring/create-default-handler
        {:not-found
         (constantly (error-page {:status 404, :title "404 - Page not found"}))
         :method-not-allowed
         (constantly (error-page {:status 405, :title "405 - Not allowed"}))
         :not-acceptable
         (constantly (error-page {:status 406, :title "406 - Not acceptable"}))}))))

(defn app []
  (middleware/wrap-base #'app-routes))
