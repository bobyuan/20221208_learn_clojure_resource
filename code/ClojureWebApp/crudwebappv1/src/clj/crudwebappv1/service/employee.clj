(ns crudwebappv1.service.employee
  (:require
   [crudwebappv1.db.core :refer [*db*] :as db]
   [next.jdbc :as jdbc]
   [java-time.local]
   [clojure.string]))



(defn get-all-employees
  "Get all employees from DB, return an empty list if no record found."
  []
  (jdbc/with-transaction [t-conn *db*]
    (if-let [records-in-db (db/get-all-employees t-conn {})]
      records-in-db
      [])))

(defn save-employee!
  "Save employee to DB by given properties (provided in params map).
   Perform update operation if ID is given, or create operation otherwise."
  [params]
  (assert (not (clojure.string/blank? (:full_name params))) "full_name cannot be empty.")
  (jdbc/with-transaction [t-conn *db*]
    (if (nil? (get params :emp_id))
      (db/create-employee! t-conn params)
      (db/update-employee! t-conn params))))

(defn delete-employee-by-id!
  "Delete an employee in DB by given emp_id (provided in params map)."
  [params]
  (assert (not (nil? (:emp_id params))) "emp_id must be provided.")
  (jdbc/with-transaction [t-conn *db*]
    (db/delete-employee! t-conn params)))

(defn find-employee-by-id
  "Find an employee record in DB by given emp_id (provided in params map).
   Return nil if not found."
  [params]
  (assert (not (nil? (:emp_id params))) "emp_id must be provided.")
  (jdbc/with-transaction [t-conn *db*]
    (db/get-employee t-conn params)))


(defn init-db
  "Initializa database, load the sample data."
  []
  (jdbc/with-transaction [t-conn *db*]
    (db/create-employee-with-id! t-conn
                                 {:emp_id 1
                                  :full_name "张成仙"
                                  :position "Manager"
                                  :office "Chongqing"
                                  :email "chenxian.zhang@outlook.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 18000.00M
                                  :birthday (java-time.local/local-date 1985 10 23)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 2
                                  :full_name "姚大山"
                                  :position "Developer"
                                  :office "Shanghai"
                                  :email "dashan.yao@hotmail.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 12000.00M
                                  :birthday (java-time.local/local-date 1992 11 3)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 3
                                  :full_name "王玛莉"
                                  :position "Tester"
                                  :office "Shanghai"
                                  :email "marry.wang@hotmail.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 13000.00M
                                  :birthday (java-time.local/local-date 1991 2 25)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 4
                                  :full_name "吴丽莉"
                                  :position "Tester"
                                  :office "Wuhan"
                                  :email "jolenewu@hotmail.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 11000.00M
                                  :birthday (java-time.local/local-date 1993 6 21)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 5
                                  :full_name "李栋"
                                  :position "Sales"
                                  :office "Chendu"
                                  :email "dong.li@hotmail.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 10000.00M
                                  :birthday (java-time.local/local-date 1992 11 8)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 6
                                  :full_name "蒋媛"
                                  :position "Developer"
                                  :office "Chendu"
                                  :email "jiang.yuan@hotmail.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 12000.00M
                                  :birthday (java-time.local/local-date 1992 3 9)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 7
                                  :full_name "陈家欣"
                                  :position "Support"
                                  :office "Xinjiang"
                                  :email "jiaxin.chen@outlook.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 13800.00M
                                  :birthday (java-time.local/local-date 1986 1 6)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 8
                                  :full_name "朱涛奋"
                                  :position "Support"
                                  :office "Chongqing"
                                  :email "taofenzhu@outlook.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 11500.00M
                                  :birthday (java-time.local/local-date 1980 10 29)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 9
                                  :full_name "张晓婷"
                                  :position "Marketing"
                                  :office "Shanghai"
                                  :email "xiaoting.zhang@hotmail.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 14600.00M
                                  :birthday (java-time.local/local-date 1980 10 29)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 10
                                  :full_name "唐峰"
                                  :position "Developer"
                                  :office "Shanghai"
                                  :email "feng.tang@outlook.com"
                                  :gender "M"
                                  :is_active false
                                  :salary 12500.00M
                                  :birthday (java-time.local/local-date 1991 11 8)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 11
                                  :full_name "胡洁琼"
                                  :position "Sales"
                                  :office "Chendu"
                                  :email "jieqiong.hu@live.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 14200.00M
                                  :birthday (java-time.local/local-date 1977 12 13)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 12
                                  :full_name "唐山姆"
                                  :position "Developer"
                                  :office "Chendu"
                                  :email "sam.tang@outlook.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 11500.00M
                                  :birthday (java-time.local/local-date 1991 11 14)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 13
                                  :full_name "祝枝山"
                                  :position "Developer"
                                  :office "Wuhan"
                                  :email "zhishan.zhu@outlook.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 12500.00M
                                  :birthday (java-time.local/local-date 1992 3 29)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 14
                                  :full_name "孙子乔"
                                  :position "Developer"
                                  :office "Wuhan"
                                  :email "ziqiao.sun@live.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 12800.00M
                                  :birthday (java-time.local/local-date 1992 11 30)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 15
                                  :full_name "赵帅"
                                  :position "Developer"
                                  :office "Wuhan"
                                  :email "shuai.zhao@live.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 12700.00M
                                  :birthday (java-time.local/local-date 1996 10 8)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 16
                                  :full_name "史珍香"
                                  :position "Tester"
                                  :office "Yunnan"
                                  :email "zhenxiang.shi@hotmail.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 10200.00M
                                  :birthday (java-time.local/local-date 1995 11 27)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 17
                                  :full_name "柳残阳"
                                  :position "Developer"
                                  :office "Yunnan"
                                  :email "canyang.liu@hotmail.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 13200.00M
                                  :birthday (java-time.local/local-date 1992 10 7)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 18
                                  :full_name "赵中翔"
                                  :position "Developer"
                                  :office "Yunnan"
                                  :email "zhongxiang.zhao@live.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 13600.00M
                                  :birthday (java-time.local/local-date 1991 9 19)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 19
                                  :full_name "赵丽娜"
                                  :position "Tester"
                                  :office "Guizhou"
                                  :email "zhao.lina@outlook.com"
                                  :gender "F"
                                  :is_active false
                                  :salary 11200.00M
                                  :birthday (java-time.local/local-date 1995 12 23)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 20
                                  :full_name "张中华"
                                  :position "Developer"
                                  :office "Henan"
                                  :email "zhangzhonghua@outlook.com"
                                  :gender "F"
                                  :is_active false
                                  :salary 14800.00M
                                  :birthday (java-time.local/local-date 1987 10 4)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 21
                                  :full_name "李开心"
                                  :position "Developer"
                                  :office "Guangzhou"
                                  :email "kaixin.lee@hotmail.com"
                                  :gender "M"
                                  :is_active true
                                  :salary 12800.00M
                                  :birthday (java-time.local/local-date 1992 11 8)
                                  :notes ""})
    (db/create-employee-with-id! t-conn
                                 {:emp_id 22
                                  :full_name "赵丁玲"
                                  :position "Developer"
                                  :office "Guangzhou"
                                  :email "dingling.zhao@live.com"
                                  :gender "F"
                                  :is_active true
                                  :salary 14600.00M
                                  :birthday (java-time.local/local-date 1987 3 12)
                                  :notes ""})))