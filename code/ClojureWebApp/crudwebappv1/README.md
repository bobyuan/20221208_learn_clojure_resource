# crudwebappv1

This is an example CRUD (create-read-update-delete) example web application.

generated using Luminus version "4.47"

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To initialize database:

```shell
lein run migrate
```

To start a web server for the application, run:

```shell
lein run
```

Open a browser to:  http://localhost:3000/

## License

Copyright © 2022 Bob YUAN
