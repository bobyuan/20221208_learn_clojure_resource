(ns fibseqv1.core
  (:gen-class))

(defn fib [index]
  (cond (= index 0) 0
        (= index 1) 1
        (= index 2) 1
        :else (+ (fib (- index 1)) (fib (- index 2)))))

(def memoize-fib (memoize fib))

(defn take-fib-seq [count]
  (take count (map memoize-fib (range 1 (inc count)))))

(defn -main
  "Calculate Fibonacci sequence."
  [& args]
  (println "Please input N (1~90): ")
  (let [count (Integer/parseInt (read-line))]
    (println (take-fib-seq count))))
