(ns fibseqv2.core
  (:gen-class))

(defn fib-seq-lazy
  "Returns a lazy sequence of Fibonacci numbers"
  ([]
   (fib-seq-lazy 0 1))
  ([a b]
   (lazy-seq
    (cons b (fib-seq-lazy b (+ a b))))))

(defn take-fib-seq [count]
  (take count (fib-seq-lazy)))

(defn -main
  "Calculate Fibonacci sequence."
  [& args]
  (println "Please input N (1~90): ")
  (let [count (Integer/parseInt (read-line))]
    (println (take-fib-seq count))))