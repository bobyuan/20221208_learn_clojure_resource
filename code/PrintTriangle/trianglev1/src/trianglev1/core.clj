(ns trianglev1.core
  (:gen-class))


(defn get-num-stars
  "get number of stars at given row index."
  [i]
  (inc (* 2 i)))

(defn get-num-spaces
  "get number of spaces at given row index."
  [n, i]
  (- n i))

;(get-num-stars 1)
;(get-num-spaces 5 1)


(defn repeated-chars
  "return a string with repeated number of given character." 
  [chr num]
  (apply str (take num (repeat chr))))

;(repeated-chars "*" 6)


(defn get-line-str [n, i]
  (let [num-space (get-num-spaces n i)
        num-star  (get-num-stars i)]
    (apply str [(repeated-chars " " num-space) (repeated-chars "*" num-star)])))

;(get-line-str 5 2)


(defn print-triangle [n]
  (loop [i 0]
    (when (< i n)
      (println (get-line-str n i))
      (recur (inc i)))))

;(print-triangle 5)


(defn -main
  "print a triangle."
  [& args]
  (println "Please input a number (1~100): ")
  (let [n (Integer/parseInt (read-line))]
    (print-triangle n)))
