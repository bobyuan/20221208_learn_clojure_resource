(ns calcpiclj.core
  (:gen-class))

(defn compute_pi [n_terms]
  (let [numerator 4.0]
    (loop [i 0
           pi 0.0
           denominator 1.0
           sign 1.0]
      (if (< i n_terms)
        (recur
         (inc i)
         (+ pi (* sign (/ numerator denominator)))
         (+ 2.0 denominator)
         (* -1 sign))
        pi))))

(compute_pi 100)

;; get elapsed time in seconds:
;; https://clojuredocs.org/clojure.core/time
(defmacro sectime
  [expr]
  `(let [start# (. System (currentTimeMillis))
         ret# ~expr]
     (prn (str "Elapsed time: " (/ (double (- (. System (currentTimeMillis)) start#)) 1000.0) " secs"))
     ret#))


(defn -main
  "Calculate pie"
  [& args]
  (let [n_terms 10000000000
        pi (sectime (compute_pi n_terms))]
    (prn pi)))

