package com.example.core;

import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Component
public class CalcPi {

	public double computePi(long n_terms) {
		double numerator = 4.0;
		double denominator = 1.0;
		double sign = 1.0;
		double pi = 0.0;

		for (long i = 0; i < n_terms; i++) {
			pi += sign * (numerator / denominator);
			denominator += 2.0;
			sign = -sign;
		}

		return pi;
	}

	public void run(String[] args) {
		long n_terms = 10000000000L;
		
		StopWatch sw = new StopWatch("computePi");
		sw.start();
		double pi = computePi(n_terms);
		sw.stop();
		
		System.out.println(pi);
		System.out.println("Execution Time: " + sw.getTotalTimeSeconds() + " seconds");
	}

}
