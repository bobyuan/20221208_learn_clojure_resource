package com.example;

import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

import com.example.core.CalcPi;

@SpringBootApplication
public class CalcPiSpringBootApplication {

	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(CalcPiSpringBootApplication.class);
        ApplicationContext appContext = builder.web(WebApplicationType.NONE).headless(false).bannerMode(Banner.Mode.OFF).run(args);

        // run this application.
        CalcPi bean = appContext.getBean(CalcPi.class);
        bean.run(args);
	}

}
