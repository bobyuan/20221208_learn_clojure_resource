# README

利用公式计算圆周率pi的近似值。
用 Java 语言实现，用到了 SpringBoot。



可以直接运行：

```shell
mvn spring-boot:run

# 运行结果：
3.141592653488346
Execution Time: 11.2853051 seconds
```



也可以打包后再运行：

```shell
mvn package
java -jar target/CalcPiSpringBoot-0.0.1-SNAPSHOT.jar

# 运行结果：
3.141592653488346
Execution Time: 22.136312799 seconds
```



