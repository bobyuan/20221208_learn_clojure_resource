// CalcPiC.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <chrono>

using namespace std;

typedef std::numeric_limits< double > dbl;

double computePi(long n_terms) {
    double numerator = 4.0f;
    double denominator = 1.0f;
    double sign = 1.0;
    double pi = 0.0;

    for (long i = 0; i < n_terms; i++) {
        pi += sign * (numerator / denominator);
        denominator += 2.0;
        sign = -sign;
    }

    return pi;
}

int main()
{
    const long n_terms = 10000000000;

    auto start = chrono::steady_clock::now();
    double pi = computePi(n_terms);
    auto end = chrono::steady_clock::now();

    std::cout.precision(dbl::max_digits10 + 2);
    std::cout << std::fixed << pi << std::endl;
    std::cout << "Elapsed time: "
        << chrono::duration_cast<chrono::seconds>(end - start).count()
        << " seconds";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
