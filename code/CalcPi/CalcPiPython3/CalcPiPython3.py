#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
利用公式计算圆周率pi的近似值

软件环境： Python 3.9.12 on Windows 10.
运行结果：

3.141592653488346
Execution Time: 1713.4678 seconds

@author bobyuan
"""

import time


def compute_pi(n_terms: int) -> float:
    numerator: float = 4.0
    denominator: float = 1.0
    sign: float = 1.0
    pi: float = 0.0
    for _ in range(n_terms):
        pi += sign * (numerator / denominator)
        denominator += 2.0
        sign = -sign
    return pi

# main entry.
if __name__ == '__main__':
    n_terms: int = 10000000000
    
    start_time = time.perf_counter()
    pi: float = compute_pi(n_terms)
    end_time = time.perf_counter()
    elapsed_time = round(end_time - start_time, 4)
    
    print(pi)
    print("Execution Time: {0} seconds".format(elapsed_time))

