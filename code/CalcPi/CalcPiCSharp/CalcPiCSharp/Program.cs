﻿// See https://aka.ms/new-console-template for more information

double computePi(long n_terms)
{
    double numerator = 4.0;
    double denominator = 1.0;
    double sign = 1.0;
    double pi = 0.0;

    for (long i = 0; i < n_terms; i++)
    {
        pi += sign * (numerator / denominator);
        denominator += 2.0;
        sign = -sign;
    }

    return pi;
}

const long n_terms = 10000000000;

long start = Environment.TickCount;
double pi = computePi(n_terms);
long end = Environment.TickCount;
long totalTicks = (end - start) / 1000;

Console.WriteLine($"{pi}");
Console.WriteLine($"Elapsed time: {totalTicks} seconds");