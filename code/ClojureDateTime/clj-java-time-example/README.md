# README

Example of using [Clojure.Java-Time](https://github.com/dm3/clojure.java-time) in Clojure.

Create this project, and change directory to the project.

```shell
lein new app clj-java-time-example
cd clj-java-time-example
```

Add the following in dependencies in `project.clj`.

```clojure
:dependencies [[org.clojure/clojure "1.11.1"]
               [clojure.java-time "1.1.0"]]
```

Download dependencies, and then start REPL.

```shell
lein deps
lein repl
```

It is now ready to use this library in the REPL.

```clojure
nREPL server started on port 64493 on host 127.0.0.1 - nrepl://127.0.0.1:64493
REPL-y 0.5.1, nREPL 0.9.0
Clojure 1.11.1
OpenJDK 64-Bit Server VM 17.0.5+8-jvmci-22.3-b08
    Docs: (doc function-name-here)
          (find-doc "part-of-name-here")
  Source: (source function-name-here)
 Javadoc: (javadoc java-object-or-class-here)
    Exit: Control+D or (exit) or (quit)
 Results: Stored in vars *1, *2, *3, an exception in *e

clj-java-time-example.core=> (require '[java-time.api :as jt]
                        #_=>          'java-time.repl)
nil

clj-java-time-example.core=> (jt/local-date)
#object[java.time.LocalDate 0x7ef8d6c "2022-12-14"]

clj-java-time-example.core=> (jt/local-date-time)
#object[java.time.LocalDateTime 0x36aeb106 "2022-12-14T22:29:52.262860300"]
```

