(ns numguessv2.core
  (:gen-class))


;;-----------------------------------------------------------------------------
;;  Parse and Format functions

(defn parse-int
  "Parse an Integer. You can give an optional default value if parse failed.
   Note: The primitive int (32 bit) variable can hold values in the range from 
   -2,147,483,648 (Integer/MIN_VALUE) to 2,147,483,647 (Integer/MAX_VALUE) inclusive."
  ([text] (java.lang.Integer/parseInt text))
  ([text default-value]
   (try
     (java.lang.Integer/parseInt text)
     (catch java.lang.NumberFormatException ne default-value))))


(defn first-char-ucase
  "return the first character (as string) of the given str, convert to upper case."
  [str]
  (clojure.string/upper-case (first str)))


;;-----------------------------------------------------------------------------
;; difficulty level handling

;; difficulty level to number of steps map.
(def diff-level-to-steps-map {"E" 30
                              "N" 20
                              "H" 10})


(defn get-maxstep
  "get user input for a difficulty level."
  []
  (println "Choose a difficulty level (E-easy,N-normal,H-hard):")
  (let [c (first-char-ucase (read-line))]
    (if (contains? diff-level-to-steps-map c)
      (get diff-level-to-steps-map c)
      (do
        (println "Invalid choice, please try again.")
        (recur)))))


;;-----------------------------------------------------------------------------
;; play the game


(defn input-guess-num
  "get user input of the guessing number."
  []
  (println "Input your guess:")
  (let [input-int (parse-int (read-line) -1)]
    (if (and (>= input-int 0) (< input-int 100))
      input-int
      (do
        (println "Invalid input, please try again.")
        (recur)))))


(defn print-banner []
  (println "*********************************")
  (println "      NumGuess Game V2")
  (println "*********************************")
  (println "Computer will randomly pick an integer from 0 to 100.")
  (println "Try to guess it out in limited steps to win.")
  (println))


(defn run-game []
  (let [maxstep (get-maxstep)
        number (rand-int 100)]
    (loop [counter 1]
      (when (<= counter maxstep)
        (let [guess (input-guess-num)]
          (cond
            (< guess number) (println "Too Low!")
            (> guess number) (println "Too Big!")
            :else (do
                    (println "You have tried" counter "times to get it! Congratulations!")
                    (System/exit 0))))
        (recur (inc counter))))
    (println "You have exhausted maximun" maxstep "steps, you lose!")))


(defn -main
  "Number guessing game."
  [& args]
  (print-banner)
  (run-game))