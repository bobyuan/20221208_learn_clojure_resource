(ns numguessv1.core
    (:gen-class))

(defn print-banner []
    (println "*********************************")
    (println "      NumGuess Game V1")
    (println "*********************************")
    (println "Computer will randomly pick an integer from 0 to 100.")
    (println "Try to guess it out in limited steps to win.")
    (println))

(defn run-game []
    (let [number (rand-int 100)]
        (loop []
            (println "Enter a guess:")
            (let [guess (Integer/parseInt (read-line))]
                (cond (> number guess)
                      (do (println "Too Low!")
                          (recur))
                      (< number guess)
                      (do (println "Too Big!")
                          (recur))
                      :else
                      (println "Yeah!"))))))

(defn -main
    "Number guessing game."
    [& args]
    (print-banner)
    (run-game))