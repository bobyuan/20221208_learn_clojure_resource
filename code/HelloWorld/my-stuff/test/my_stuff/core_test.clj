(ns my-stuff.core-test
  (:require [clojure.test :refer :all]
            [my-stuff.core :refer :all]))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 1 1))))

(deftest my-average-test
  (testing "Unit test of my-average function"
    (is (= 4.0 (my-average 3 5)))
    (is (= 0.0 (my-average 0 0)))
    (is (= (* 1.0 Long/MAX_VALUE) (my-average Long/MAX_VALUE Long/MAX_VALUE)))))