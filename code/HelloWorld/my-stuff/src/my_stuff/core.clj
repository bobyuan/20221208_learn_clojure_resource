(ns my-stuff.core
  (:gen-class))

(defn my-average [a b]
    (+ (/ a 2.0) (/ b 2.0)))
    
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
